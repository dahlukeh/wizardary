package org.bitbucket.dahlukeh.wizardary.terrainmaker;

import java.util.List;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;

public class SpawnMaker {

	private World w;
	private int width;
	private int height;
	private Location lowerLeftBack;
	private Location upperRightFront;
	private Location newSpawn;

	public SpawnMaker(World w) {
		this.w = w;
		width = 5;
		height = 2;
	}

	public void make(Location l, List<String> classes) {
		int upTo = 0;
		for (int i = -width; i <= width; i++) {
			for (int j = -height; j <= height; j++) {
				for (int k = -width; k <= width; k++) {
					Location newL = l.clone().add(i, j, k);
					Block b = w.getBlockAt(newL);
					if (Math.abs(i) == width || Math.abs(j) == height || Math.abs(k) == width) {
						b.setType(Material.BEDROCK);
					} else {
						if (j == -height + 1 && (i + width) % 2 == 1 && (k + width) % 2 == 1) {
							b.setType(Material.TORCH);
							b.setData((byte) 5);
						} else {
							if (j == 0 && upTo < classes.size()) {
								if (Math.abs(i) == width - 1 && (k + width) % 3 == 2) {
									b.setType(Material.WALL_SIGN);
									Sign state = (Sign) b.getState();
									if (i == width - 1) {
										state.setRawData((byte) 4);
									} else {
										state.setRawData((byte) 5);
									}
									state.setLine(1, classes.get(upTo));
									upTo++;
									state.update(true);
								} else if (Math.abs(k) == width - 1 && (i + width) % 3 == 2) {
									b.setType(Material.WALL_SIGN);
									Sign state = (Sign) b.getState();
									if (k == width - 1) {
										state.setRawData((byte) 2);
									} else {
										state.setRawData((byte) 3);
									}
									state.setLine(1, classes.get(upTo));
									upTo++;
									state.update(true);
								} else {
									b.setType(Material.AIR);
								}
							} else {
								b.setType(Material.AIR);
							}
						}
					}
				}
			}
		}
		lowerLeftBack = l.clone().add(-width, -height, -width);
		upperRightFront = l.clone().add(width, height, width);
		newSpawn = l.clone().add(0, -height + 1, 0);
	}

	public Location getNewSpawn() {
		return newSpawn;
	}
	
	public boolean isInArea (Location l) {
		if (l.getBlockX() >= lowerLeftBack.getBlockX() && l.getBlockX() <= upperRightFront.getBlockX()) {
			if (l.getBlockY() >= lowerLeftBack.getBlockY() && l.getBlockY() <= upperRightFront.getBlockY()) {
				if (l.getBlockZ() >= lowerLeftBack.getBlockZ() && l.getBlockZ() <= upperRightFront.getBlockZ()) {
					return true;
				}
			}
		}
		return false;
	}

}
