package org.bitbucket.dahlukeh.wizardary;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import org.bitbucket.dahlukeh.wizardary.classes.Person;
import org.bitbucket.dahlukeh.wizardary.gameinfo.GameInfo;
import org.bitbucket.dahlukeh.wizardary.gameinfo.GameInformation;
import org.bitbucket.dahlukeh.wizardary.scheduler.Scheduler;
import org.bitbucket.dahlukeh.wizardary.terrainmaker.SpawnMaker;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Sign;
import org.bukkit.entity.Creature;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.entity.EntityShootBowEvent;
import org.bukkit.event.entity.EntityTargetEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerRespawnEvent;


public class PersonPool implements Listener {

	private PersonFactory creator;
	private Scheduler scheduler;
	private SpawnMaker maker;
	private Random r;
	private Map<Player, Person> people;
	private GameInfo info;

	public PersonPool(PersonFactory creator, Scheduler scheduler, SpawnMaker maker) {
		this.creator = creator;
		r = new Random();
		this.scheduler = scheduler;
		this.maker = maker;
		people = new HashMap<Player, Person>();
		info = new GameInformation();
	}

	@EventHandler
	public void playerJoin(PlayerJoinEvent event) {
		getPerson(event.getPlayer());
		
		event.getPlayer().teleport(getSpawnLoc());
	}

	@EventHandler
	public void playerRespawn(PlayerRespawnEvent event) {
		Person p = getPerson(event.getPlayer());
		p.disposePlayer();
		people.remove(event.getPlayer());
		p = getPerson(event.getPlayer());
		event.setRespawnLocation(getSpawnLoc());
	}
	
	@EventHandler
	public void blockBroken (BlockBreakEvent event) {
		if (maker.isInArea(event.getBlock().getLocation())) {
			event.setCancelled(true);
		}
	}
	
	@EventHandler
	public void blockPlaced (BlockPlaceEvent event) {
		if (maker.isInArea(event.getBlock().getLocation())) {
			event.setBuild(false);
		}
	}
	
	@EventHandler
	public void playerClick(PlayerInteractEvent event) {
		Person p = getPerson(event.getPlayer());
		Action a = event.getAction();
		if (a == Action.LEFT_CLICK_AIR || a == Action.LEFT_CLICK_BLOCK) {
			p.leftClick(event);
		} else if (a == Action.RIGHT_CLICK_AIR || a == Action.RIGHT_CLICK_BLOCK) {
			p.rightClick(event);
			if (event.getClickedBlock() != null && event.getClickedBlock().getType() == Material.WALL_SIGN) {
				if (maker.isInArea(event.getClickedBlock().getLocation())) {
					String name = ((Sign) event.getClickedBlock().getState()).getLine(1);
					p.disposePlayer();
					p = getNewPerson(event.getPlayer(), name);
					people.put (event.getPlayer(), p);
					event.getPlayer().sendMessage("You are now a " + name);
					event.getPlayer().teleport(getRandomLoc(event.getPlayer().getLocation()));
				}
			}
		}
	}
	
	@EventHandler
	public void playerMoved(PlayerMoveEvent event) {
		Person p = getPerson(event.getPlayer());
		p.playerMoved(event.getFrom(), event.getTo());
	}
	
	@EventHandler
	public void playerDamages (EntityDamageByEntityEvent event) {
		if (event.getDamager() instanceof Player) {
			Player p = (Player) event.getDamager();
			Person per = getPerson(p);
			per.playerDamages((LivingEntity) event.getEntity());
		} else if (event.getDamager() instanceof Projectile) {
			Projectile proj = (Projectile) event.getDamager();
			if (proj.getShooter() instanceof Player) {
				Player p = (Player) proj.getShooter();
				Person per = getPerson(p);
				per.arrowHit(event.getEntity());
			}
		}
	}
	
	@EventHandler
	public void playerTargeted (EntityTargetEvent event) {
		if (event.getTarget() instanceof Player && event.getEntity() instanceof Creature) {
			Player p = (Player) event.getTarget();
			Person per = getPerson(p);
			event.setCancelled(!per.entityTargets((Creature) event.getEntity(), event.getReason()));
		}
	}
	
	@EventHandler
	public void damageTaken (EntityDamageEvent event) {
		if (event.getEntity() instanceof Player) {
			Player p = (Player) event.getEntity();
			Person per = getPerson(p);
			per.tookDamage(event);
		}
	}

	
	@EventHandler
	public void shotBow (EntityShootBowEvent event) {
		if (event.getEntity() instanceof Player) {
			Player p = (Player) event.getEntity();
			Person per = getPerson(p);
			per.shot(event);
		}
	}
	
	@EventHandler
	public void explosion (EntityExplodeEvent event) {
		if (event.getEntity() == null || event.getEntityType() != EntityType.CREEPER) {
			event.setYield(0);
		}
	}
	


	public boolean processCommand(Player sender, String command, String args[]) {
		boolean success = false;
		if (command.equalsIgnoreCase("level")) {
			if (args.length == 1) {
				try {
					int conv = Integer.valueOf(args[0]);
					Person p = getPerson(sender);
					if (conv <= p.getMaxLevel() && conv >= 1) {
						p.setLevel(conv);
						sender.sendMessage("You are now level " + conv);
						success = true;	
					} else {
						sender.sendMessage("You must enter a level >= 1 and <= " + p.getMaxLevel());
					}
				} catch (Exception e) {
					success = false;
				}
			}
		}
		return success;
	}

	private Person getNewPerson(Player p, String extra) {
		Person per = getPerson(p);
		per.disposePlayer();
		return creator.createPerson(p, scheduler, info, extra);
	}


	private Location getSpawnLoc() {	
		return maker.getNewSpawn();
	}
	
	private Location getRandomLoc (Location base) {
		Location newL = base.clone().add(r.nextInt(200) - 100, 0, r.nextInt(200) - 100);
		return newL.getWorld().getHighestBlockAt(newL).getLocation();
	}
	
	private Person getPerson (Player p) {
		if (people.containsKey(p)) {
			return people.get(p);
		} else {
			Person per = creator.createPerson(p, scheduler, info, "");
			info.addPlayer(p);
			people.put(p, per);
			return per;
		}
	}

}
