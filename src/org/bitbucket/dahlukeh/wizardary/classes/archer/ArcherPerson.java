package org.bitbucket.dahlukeh.wizardary.classes.archer;

import org.bitbucket.dahlukeh.wizardary.classes.BasicPerson;
import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityShootBowEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

public class ArcherPerson extends BasicPerson {

	private int level;
	private Player p;
	private Entity lastFired;
	
	public ArcherPerson (Player p) {
		level = 1;
		lastFired = null;
		this.p = p;
	}

	@Override
	public void setLevel(int level) {
		this.level = level;
	}
	
	@Override
	public int getMaxLevel() {
		return 5;
	}
	
	@Override
	public void setUpPlayer() {
		PlayerInventory inv = p.getInventory();
		clearInventory(inv);
		inv.setItem(0, new ItemStack(Material.BOW, 1));
		inv.setItem(1, new ItemStack(Material.COOKED_BEEF, 64));
		inv.setItem(2, new ItemStack(Material.DIAMOND_PICKAXE, 1));
		inv.setItem(3, new ItemStack(Material.DIAMOND_SPADE, 1));
		inv.setItem(9, new ItemStack(Material.ARROW, 64));
		inv.setBoots(new ItemStack(Material.CHAINMAIL_BOOTS));
		inv.setLeggings(new ItemStack(Material.CHAINMAIL_LEGGINGS));
		inv.setChestplate(new ItemStack(Material.CHAINMAIL_CHESTPLATE));
		inv.setHelmet(new ItemStack(Material.CHAINMAIL_HELMET));
	}

	@Override
	public void leftClick(PlayerInteractEvent event) {
		if (p.getItemInHand().getAmount() > 0 && p.getItemInHand().getType() == Material.BOW) {
			if (lastFired != null) {
				lastFired.getWorld().createExplosion(lastFired.getLocation(), 2 + level);
				lastFired.remove();
				lastFired = null;
			}
		}
	}
	
	@Override
	public void shot(EntityShootBowEvent event) {
		lastFired = event.getProjectile();
		if (p.isSneaking()) {
			event.getProjectile().setVelocity(event.getProjectile().getVelocity().multiply(2));
		}
		ItemStack is = new ItemStack(Material.ARROW, 1);
		p.getLocation().getWorld().dropItemNaturally(p.getLocation(), is);
		p.getInventory().setItem(9, new ItemStack(Material.ARROW, 64));
	}

	
}
