package org.bitbucket.dahlukeh.wizardary.classes;

import org.bukkit.Location;
import org.bukkit.entity.Creature;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityShootBowEvent;
import org.bukkit.event.entity.EntityTargetEvent.TargetReason;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.PlayerInventory;

/*
 * Every class on person should inherit from this, and override methods
 */
public class BasicPerson implements Person {

	private Player p;
	
	public BasicPerson () {
		
	}
	
	public BasicPerson (Player p) {
		this.p = p;
	}
	
	@Override
	public void setLevel(int level) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setUpPlayer() {
		clearInventory(p.getInventory());
	}
	
	@Override
	public void clearInventory (PlayerInventory inv) {
		for (int i = 0; i < inv.getSize(); i++) {
			inv.setItem(i, null);
		}
		inv.setBoots(null);
		inv.setChestplate(null);
		inv.setHelmet(null);
		inv.setLeggings(null);
	}

	@Override
	public void rightClick(PlayerInteractEvent event) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void leftClick(PlayerInteractEvent event) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void playerMoved(Location from, Location to) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void playerDamages(LivingEntity damaged) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean entityTargets(Creature e, TargetReason reason) {
		return true;
	}

	@Override
	public void tookDamage(EntityDamageEvent event) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public int getMaxLevel() {
		// TODO Auto-generated method stub
		return 1;
	}

	@Override
	public void shot(EntityShootBowEvent event) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void arrowHit(Entity hit) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void disposePlayer() {
		// TODO Auto-generated method stub
		
	}




}
