package org.bitbucket.dahlukeh.wizardary.classes.wizard;

import java.util.List;

import org.bitbucket.dahlukeh.wizardary.classes.BasicPerson;
import org.bitbucket.dahlukeh.wizardary.classes.sorcerer.LightningEffect;
import org.bitbucket.dahlukeh.wizardary.scheduler.Scheduler;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Fireball;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.util.Vector;


public class WizardPerson extends BasicPerson {

	public static final int MAX_LEVEL = 5;

	private Player p;
	private LightningEffect striker;
	private int level;
	private Scheduler sch;

	public WizardPerson(Player p, Scheduler scheduler) {
		this.p = p;
		level = 1;
		striker = null;
		sch = scheduler;
	}
	

	@Override
	public void playerMoved(Location from, Location to) {
		if (from.distanceSquared(to) > 0.01) {
			if (striker != null) {
				if (striker.stillCharging()) {
					striker.cancel();
					p.sendMessage("Charging disrupted");
				}
			}
		}
	}

	public static boolean shouldStrike(Location playerLoc, Block toStrike) {
		if (Math.abs(playerLoc.getBlockX() - toStrike.getX()) > 6
				|| Math.abs(playerLoc.getBlockZ() - toStrike.getZ()) > 6) {
			return true;
		} else {
			return false;
		}

	}

	public static void strike(World w, Block b) {
		w.strikeLightning(b.getLocation());
	}

	private Fireball createBall(Player p, World w) {
		Vector dir = p.getLocation().getDirection();
		Fireball f = w.spawn(p.getLocation().add(dir.multiply(6)), Fireball.class);
		f.setShooter(p);
		f.setVelocity(p.getLocation().getDirection().multiply(6));
		return f;
	}


	@Override
	public void setLevel(int level) {
		this.level = level;
	}
	
	@Override
	public int getMaxLevel () {
		return 5;
	}


	@Override
	public void setUpPlayer() {
		PlayerInventory inv = p.getInventory();
		clearInventory(inv);
		inv.setItem(0, new ItemStack(Material.STICK, 1));
		inv.setItem(1, new ItemStack(Material.COOKED_BEEF, 64));
		inv.setItem(2, new ItemStack(Material.DIAMOND_PICKAXE, 1));
		inv.setItem(3, new ItemStack(Material.DIAMOND_SPADE, 1));
		inv.setBoots(new ItemStack(Material.GOLD_BOOTS));
		inv.setLeggings(new ItemStack(Material.GOLD_LEGGINGS));
		inv.setChestplate(new ItemStack(Material.GOLD_CHESTPLATE));
		inv.setHelmet(new ItemStack(Material.GOLD_HELMET));
	}


	@Override
	public void leftClick(PlayerInteractEvent event) {
		World w = p.getWorld();
		Location l = p.getLocation();
		if (p.getItemInHand().getAmount() > 0 && p.getItemInHand().getType() == Material.STICK) {
			if (level == 1) {
				Fireball f = createBall(p, w);
				f.setYield(3);
				f.setFireTicks(5);
			} else if (level == 2) {
				Block b = p.getTargetBlock(null, 100);
				if (shouldStrike(l, b)) {
					for (int i = -4; i <= 4; i += 4) {
						for (int j = -4; j <= 4; j += 4) {
							Fireball f = w.spawn(b.getLocation().add(i, 40, j), Fireball.class);
							f.setShooter(p);
							f.setDirection(new Vector(0, -10, 0));
							f.setVelocity(new Vector(0, -1, 0));
							f.setYield(5);
							f.setFireTicks(1);
						}
					}
				}
			} else if (level == 3) {
				for (int i = -50; i <= 50; i += 5) {
					for (int j = -50; j <= 50; j += 5) {
						Block b = p.getLocation().add(i, 0, j).getBlock();
						if (shouldStrike(l, b)) {
							Fireball f = w.spawn(b.getLocation().add(0, 50, 0), Fireball.class);
							f.setShooter(p);
							f.setDirection(new Vector(0, -10, 0));
							f.setVelocity(new Vector(0, -0.5, 0));
							f.setYield(2);
							f.setFireTicks(20);
						}
					}
				}
			} else if (level == 4) {
				List<Entity> near = p.getNearbyEntities(15, 15, 15);
				for (Entity e : near) {
					if (shouldStrike(l, e.getLocation().getBlock())) {
						if (e instanceof LivingEntity) {
							Fireball f = w.spawn(e.getLocation().add(0, 20, 0), Fireball.class);
							f.setShooter(p);
							f.setVelocity(new Vector(0, -2, 0));
							f.setDirection(new Vector(0, -10, 0));
							f.setYield(10);
							f.setFireTicks(20);
						}
					}
				}
			} else {
				p.sendMessage("Your level is not allowed. Must be >= 1 and <= " + MAX_LEVEL);
			}
		}
	}


	@Override
	public void rightClick(PlayerInteractEvent event) {
		World w = p.getWorld();
		Location l = p.getLocation();
		if (p.getItemInHand().getAmount() > 0 && p.getItemInHand().getType() == Material.STICK) {
			if (level == 1) {
				strike(w, p.getTargetBlock(null, 200));
			} else if (level == 2) {
				List<Block> blocks = p.getLineOfSight(null, 100);

				for (Block b : blocks) {
					if (shouldStrike(l, b)) {
						strike(w, b);
					}
				}
			} else if (level == 3) {
				for (int i = -40; i <= 40; i += 2) {
					for (int j = -40; j <= 40; j += 2) {
						Block b = w.getHighestBlockAt(l.getBlockX() + i, l.getBlockZ() + j);
						if (shouldStrike(l, b)) {
							strike(w, b);
						}
					}
				}
			} else if (level == 4) {
				List<Entity> near = p.getNearbyEntities(15, 15, 15);
				for (Entity e : near) {
					if (shouldStrike(l, e.getLocation().getBlock())) {
						if (e instanceof LivingEntity) {
							strike(w, e.getLocation().getBlock());
						}
					}
				}

			} else if (level == 5) {
				if (striker == null || !striker.isRunning()) {
					striker = new LightningEffect(w, l, 75, sch, p);
					p.sendMessage("Started charging...");
				} else {
					p.sendMessage("Already charging");
				}
			} else {
				p.sendMessage("Your level is not allowed. Must be >= 1 and <= " + MAX_LEVEL);
			}
		}
	}

}
