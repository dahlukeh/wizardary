package org.bitbucket.dahlukeh.wizardary.classes.warrior;

import org.bitbucket.dahlukeh.wizardary.classes.BasicPerson;
import org.bitbucket.dahlukeh.wizardary.classes.CooldownTimer;
import org.bitbucket.dahlukeh.wizardary.scheduler.Scheduler;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

public class WarriorPerson extends BasicPerson {

	private Player p;
	private int level;
	private CooldownTimer timer;

	public WarriorPerson(Player p, Scheduler sch) {
		this.p = p;
		level = 1;
		timer = new CooldownTimer(sch);
	}

	@Override
	public void setLevel(int level) {
		this.level = level;
	}

	@Override
	public int getMaxLevel() {
		return 5;
	}

	@Override
	public void setUpPlayer() {
		PlayerInventory inv = p.getInventory();
		clearInventory(inv);
		inv.setItem(0, new ItemStack(Material.DIAMOND_SWORD, 1));
		inv.setItem(1, new ItemStack(Material.COOKED_BEEF, 64));
		inv.setItem(2, new ItemStack(Material.DIAMOND_PICKAXE, 1));
		inv.setItem(3, new ItemStack(Material.DIAMOND_SPADE, 1));
		inv.setBoots(new ItemStack(Material.DIAMOND_BOOTS));
		inv.setLeggings(new ItemStack(Material.DIAMOND_LEGGINGS));
		inv.setChestplate(new ItemStack(Material.DIAMOND_CHESTPLATE));
		inv.setHelmet(new ItemStack(Material.DIAMOND_HELMET));
	}

	@Override
	public void rightClick(PlayerInteractEvent event) {
		if (p.getItemInHand().getAmount() > 0 && p.getItemInHand().getType() == Material.DIAMOND_SWORD) {
			if (timer.isCooldowned()) {
				Location lookingat = p.getTargetBlock(null, 20).getLocation();
				lookingat.setPitch(p.getLocation().getPitch());
				lookingat.setYaw(p.getLocation().getYaw());
				p.teleport(lookingat);
				timer.start((10 - level) * 20);
			} else {
				p.sendMessage(timer.getSecondsLeft() + " seconds left to cooldown");
			}
		}
	}

}
