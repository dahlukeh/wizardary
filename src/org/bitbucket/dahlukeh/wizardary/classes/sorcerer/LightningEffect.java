package org.bitbucket.dahlukeh.wizardary.classes.sorcerer;

import java.util.Random;

import org.bitbucket.dahlukeh.wizardary.classes.wizard.WizardPerson;
import org.bitbucket.dahlukeh.wizardary.scheduler.Scheduler;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;



public class LightningEffect implements SorcererEffect {

	private World w;
	private Location pos;
	private Player myP;
	private int r;
	private int curR;
	private int curI;
	private boolean charging;
	private Random rand;
	private Scheduler sch;
	private boolean running;

	public LightningEffect(World w, Location start, int radius, Scheduler scheduler, Player p) {
		this.w = w;
		pos = start;
		r = radius;
		myP = p;
		rand = new Random();
		charging = true;
		curR = 0;
		curI = 0;
		running = true;
		sch = scheduler;
		sch.schedule(this, 0, 2);
	}

	@Override
	public void run() {
		if (curR <= r) {
			if (curI < 4) {
				boolean worked = false;
				for (int j = -curR; j <= curR; j++) {
					boolean any = false;
					if (curI == 0) {
						any = strikeAt(new Vector(-curR, 0, j));
					} else if (curI == 1) {
						any = strikeAt(new Vector(j, 0, curR));
					} else if (curI == 2) {
						any = strikeAt(new Vector(curR, 0, j));
					} else if (curI == 3) {
						any = strikeAt(new Vector(j, 0, -curR));
					}
					if (any) {
						worked = true;
					}
				}
				if (worked) {
					if (charging) {
						myP.sendMessage("You unleash a wall of thunder");
					}
					charging = false;
				}
				curI++;
			} else {
				curR++;
				curI = 0;
				if (charging) {
					for (int i = 0; i < curR * curR; i++) {
						Vector v = new Vector(rand.nextInt(30) - 15, 0, rand.nextInt(30) - 15);
						Block b = w.getHighestBlockAt(pos.clone().add(v));
						w.strikeLightningEffect(b.getLocation());
					}
					curI = -2;
				}
			}
		} else {
			cancel();
		}
	}

	public boolean strikeAt(Vector add) {
		Block b = w.getHighestBlockAt(pos.clone().add(add));
		if (WizardPerson.shouldStrike(pos, b)) {
			WizardPerson.strike(w, b);
			return true;
		} else {
			return false;
		}
	}

	public boolean stillCharging() {
		return charging;
	}

	@Override
	public void cancel() {
		charging = false;
		sch.cancel(this);
		running = false;
	}
	
	@Override
	public boolean isRunning () {
		return running;
	}

}
