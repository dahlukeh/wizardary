package org.bitbucket.dahlukeh.wizardary.classes.sorcerer;

import org.bitbucket.dahlukeh.wizardary.classes.BasicPerson;
import org.bitbucket.dahlukeh.wizardary.classes.CooldownTimer;
import org.bitbucket.dahlukeh.wizardary.scheduler.Scheduler;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Biome;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.entity.SmallFireball;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.util.Vector;

public class SorcererPerson extends BasicPerson {

	private Player p;
	private int level;
	private Scheduler sch;
	private CooldownTimer timer;
	private SorcererEffect curEffect;

	public SorcererPerson(Player p, Scheduler sch) {
		this.p = p;
		level = 1;
		this.sch = sch;
		timer = new CooldownTimer(sch);
		curEffect = null;
	}

	@Override
	public void setLevel(int level) {
		this.level = level;
		setUpPlayer();
	}

	@Override
	public int getMaxLevel() {
		return 5;
	}

	@Override
	public void setUpPlayer() {
		PlayerInventory inv = p.getInventory();
		clearInventory(inv);
		inv.setItem(0, new ItemStack(Material.DIAMOND_SWORD, 1));
		inv.setItem(0, new ItemStack(Material.BLAZE_ROD, 1));
		inv.setItem(1, new ItemStack(Material.COOKED_BEEF, 64));
		inv.setItem(2, new ItemStack(Material.DIAMOND_PICKAXE, 1));
		inv.setItem(3, new ItemStack(Material.DIAMOND_SPADE, 1));
		if (level >= 1) {
			inv.setItem(4, new ItemStack(Material.SNOW_BALL, 1));
		}
		if (level >= 2) {
			inv.setItem(5, new ItemStack(Material.FLINT_AND_STEEL, 1));
		}
		if (level >= 3) {
			inv.setItem(6, new ItemStack(Material.STICK, 1));
		}
		if (level >= 4) {
			inv.setItem(7, new ItemStack(Material.BOAT, 1));
		}
		if (level >= 5) {
			inv.setItem(8, new ItemStack(Material.FIREBALL, 1));
		}
		inv.setHelmet(new ItemStack(Material.GOLD_HELMET, 1));
	}

	@Override
	public void disposePlayer() {
		if (curEffect != null && curEffect.isRunning()) {
			curEffect.cancel();
		}
	}

	@Override
	public void rightClick(PlayerInteractEvent event) {
		if (p.getItemInHand().getAmount() > 0 && p.getItemInHand().getType() == Material.BLAZE_ROD) {
			Vector dir = p.getLocation().getDirection().normalize();
			SmallFireball f = p.getWorld().spawn(p.getLocation().add(0, 2, 0), SmallFireball.class);
			f.setShooter(p);
			f.setVelocity(dir.multiply(3));
		} else if (p.getItemInHand().getAmount() > 0 && p.getItemInHand().getType() == Material.SNOW_BALL) {
			if (timer.isCooldowned()) {
				World w = p.getWorld();
				for (int i = -100; i <= 100; i++) {
					for (int j = -100; j <= 100; j++) {
						w.setBiome(p.getLocation().getBlockX() + i, p.getLocation().getBlockZ() + j, Biome.ICE_PLAINS);

					}
				}
				w.setStorm(true);
				w.setWeatherDuration(20 * 30);
				timer.start(30 * 20);
			} else {
				p.sendMessage(timer.getSecondsLeft() + " seconds left to cooldown");
			}
			event.setCancelled(true);
		} else if (p.getItemInHand().getAmount() > 0 && p.getItemInHand().getType() == Material.FLINT_AND_STEEL) {
			if (timer.isCooldowned()) {
				curEffect = new FireEffect(p.getLocation(), sch, 20 * 30);
				timer.start(30 * 20);
			} else {
				p.sendMessage(timer.getSecondsLeft() + " seconds left to cooldown");
			}
			event.setCancelled(true);
		} else if (p.getItemInHand().getAmount() > 0 && p.getItemInHand().getType() == Material.STICK) {
			if (timer.isCooldowned()) {
				curEffect = new LightningEffect(p.getWorld(), p.getLocation(), 50, sch, p);
				timer.start(30 * 20);
			} else {
				p.sendMessage(timer.getSecondsLeft() + " seconds left to cooldown");
			}
		} else if (p.getItemInHand().getAmount() > 0 && p.getItemInHand().getType() == Material.BOAT) {
			if (timer.isCooldowned()) {
				curEffect = new WaterEffect(p.getLocation(), sch, 20 * 30);
				timer.start(30 * 20);
			} else {
				p.sendMessage(timer.getSecondsLeft() + " seconds left to cooldown");
			}
			event.setCancelled(true);
		} else if (p.getItemInHand().getAmount() > 0 && p.getItemInHand().getType() == Material.FIREBALL) {
			if (timer.isCooldowned()) {
				curEffect = new FireBallEffect(p.getLocation(), sch, 20 * 30);
				timer.start(30 * 20);
			} else {
				p.sendMessage(timer.getSecondsLeft() + " seconds left to cooldown");
			}
			event.setCancelled(true);
		}
	}

	@Override
	public void leftClick(PlayerInteractEvent event) {
		if (p.getItemInHand().getAmount() > 0 && p.getItemInHand().getType() == Material.BLAZE_ROD) {
			Block b = p.getTargetBlock(null, 200);
			b.setType(Material.FIRE);
		}
	}

}
