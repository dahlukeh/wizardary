package org.bitbucket.dahlukeh.wizardary.classes.sorcerer;

import java.util.Random;

import org.bitbucket.dahlukeh.wizardary.scheduler.Scheduler;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;

public class FireEffect implements SorcererEffect {

	private Location root;
	private Scheduler sch;
	private int timesLeft;
	private Random r;
	
	public FireEffect (Location root, Scheduler sch, int run) {
		this.root = root;
		this.sch = sch;
		sch.schedule(this, 0, 20);
		this.timesLeft = run;
		r = new Random();
	}
	
	@Override
	public void run() {
		for (int i = 0; i < timesLeft * 10; i++) {
			Location newL = root.clone().add(r.nextInt(100) - 50, 0, r.nextInt(100) - 50);
			Block b = root.getWorld().getBlockAt(newL.getBlockX(), (root.getWorld().getHighestBlockYAt(newL)) + 1, newL.getBlockZ());
			b.setType(Material.FIRE);
			b.setData((byte) 0);
		}
		root.getWorld().strikeLightningEffect(root);
		timesLeft -= 20;
		if (timesLeft <= 0) {
			cancel();
		}
	}
	
	
	@Override
	public void cancel () {
		sch.cancel(this);
	}

	@Override
	public boolean isRunning() {
		return timesLeft > 0;
	}

	
}
