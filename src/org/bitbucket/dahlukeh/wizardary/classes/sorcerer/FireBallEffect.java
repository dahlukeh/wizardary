package org.bitbucket.dahlukeh.wizardary.classes.sorcerer;

import java.util.Random;

import org.bitbucket.dahlukeh.wizardary.scheduler.Scheduler;
import org.bukkit.Location;
import org.bukkit.entity.Fireball;
import org.bukkit.util.Vector;

public class FireBallEffect implements SorcererEffect {

	
	private Location root;
	private Scheduler sch;
	private int timesLeft;
	private Random r;
	
	public FireBallEffect (Location root, Scheduler sch, int run) {
		this.root = root;
		this.sch = sch;
		sch.schedule(this, 0, 20);
		this.timesLeft = run;
		r = new Random();
	}
	
	@Override
	public void run() {
		for (int i = 0; i <= timesLeft / 20; i++) {
			Location newL = root.clone().add(r.nextInt(100) - 50, 0, r.nextInt(100) - 50);
			Fireball f = newL.getWorld().spawn(newL.add(0, 50, 0), Fireball.class);
			f.setVelocity(new Vector(0, -6, 0));
			f.setDirection(new Vector(0, -10, 0));
			f.setYield(3);
			f.setFireTicks(2);
		}
		root.getWorld().strikeLightningEffect(root);
		timesLeft -= 20;
		if (timesLeft <= 0) {
			cancel();
		}
	}
	
	
	@Override
	public void cancel () {
		sch.cancel(this);
	}

	@Override
	public boolean isRunning() {
		return timesLeft > 0;
	}

}
