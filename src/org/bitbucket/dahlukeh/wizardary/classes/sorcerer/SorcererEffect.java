package org.bitbucket.dahlukeh.wizardary.classes.sorcerer;

public interface SorcererEffect extends Runnable {

	public void cancel ();
	
	public boolean isRunning ();
	
}
