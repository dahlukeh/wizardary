package org.bitbucket.dahlukeh.wizardary.classes.sorcerer;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.bitbucket.dahlukeh.wizardary.scheduler.Scheduler;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;

public class WaterEffect implements SorcererEffect {

	private List<Location> waterLocation;
	private Location root;
	private Scheduler sch;
	private int timesLeft;
	private Random r;
	
	public WaterEffect (Location root, Scheduler sch, int run) {
		this.root = root;
		this.sch = sch;
		sch.schedule(this, 0, 20);
		this.timesLeft = run;
		waterLocation = new ArrayList<Location>();
		r = new Random();
	}
	
	@Override
	public void run() {
		for (int i = 0; i < timesLeft / 10; i++) {
			Location newL = root.clone().add(r.nextInt(100) - 50, 0, r.nextInt(100) - 50);
			newL.setY(root.getWorld().getHighestBlockYAt(newL) + 1);
			Block b = root.getWorld().getBlockAt(newL);
			b.setType(Material.WATER);
			waterLocation.add(newL);
		}
		root.getWorld().strikeLightningEffect(root);
		timesLeft -= 20;
		if (timesLeft <= 0) {
			cancel();
		}
	}
	
	
	@Override
	public void cancel () {
		for (Location l: waterLocation) {
			l.getWorld().getBlockAt(l).setType(Material.AIR);
		}
		sch.cancel(this);
	}

	@Override
	public boolean isRunning() {
		return timesLeft > 0;
	}
}
