package org.bitbucket.dahlukeh.wizardary.classes;

import org.bukkit.Location;
import org.bukkit.entity.Creature;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityShootBowEvent;
import org.bukkit.event.entity.EntityTargetEvent.TargetReason;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.PlayerInventory;

public interface Person {

	public void setLevel(int level);

	public int getMaxLevel();

	public void setUpPlayer();

	public void clearInventory(PlayerInventory inv);

	public void disposePlayer();

	public void rightClick(PlayerInteractEvent event);

	public void leftClick(PlayerInteractEvent event);

	public void playerMoved(Location from, Location to);

	public void playerDamages(LivingEntity damaged);

	/*
	 * Returns whether or not to allow
	 */
	public boolean entityTargets(Creature e, TargetReason reason);

	public void tookDamage(EntityDamageEvent event);

	public void shot(EntityShootBowEvent event);

	public void arrowHit(Entity hit);

}
