package org.bitbucket.dahlukeh.wizardary.classes;

import org.bitbucket.dahlukeh.wizardary.scheduler.Scheduler;

public class CooldownTimer implements Runnable {

	private Scheduler sch;
	private int timeLeft;
	
	public CooldownTimer (Scheduler sch) {
		timeLeft = 0;
		this.sch = sch;
	}
	
	@Override
	public void run() {
		if (timeLeft == 0) {
			sch.cancel(this);
		} else {
			timeLeft--;
		}
	}
	
	public boolean isCooldowned() {
		return timeLeft == 0;
	}
	
	public void start (int time) {
		timeLeft = time;
		sch.schedule(this, 0, 1);
	}
	
	public int getTimeLeft () {
		return timeLeft;
	}
	
	public int getSecondsLeft () {
		return timeLeft / 20;
	}
	
	
	

	
	
}
