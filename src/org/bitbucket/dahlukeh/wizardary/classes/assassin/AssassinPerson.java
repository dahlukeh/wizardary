package org.bitbucket.dahlukeh.wizardary.classes.assassin;

import org.bitbucket.dahlukeh.wizardary.classes.BasicPerson;
import org.bitbucket.dahlukeh.wizardary.classes.CooldownTimer;
import org.bitbucket.dahlukeh.wizardary.gameinfo.GameInfo;
import org.bitbucket.dahlukeh.wizardary.scheduler.Scheduler;
import org.bukkit.Material;
import org.bukkit.entity.Creature;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.entity.EntityTargetEvent.TargetReason;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class AssassinPerson extends BasicPerson {

	private Player p;
	private GameInfo info;
	private Scheduler sch;
	private int level;
	private InvisiblityEffect effect;
	private CooldownTimer timer;

	public AssassinPerson(Player p, Scheduler sch, GameInfo info) {
		this.p = p;
		this.sch = sch;
		this.info = info;
		level = 1;
		effect = null;
		timer = new CooldownTimer(sch);
	}
	

	@Override
	public void setLevel(int level) {
		this.level = level;
	}
	
	@Override
	public int getMaxLevel() {
		return 5;
	}
	
	@Override
	public void disposePlayer() {
		if (effect != null && effect.isRunning()) {
			effect.cancel();
		}
	}

	@Override
	public void setUpPlayer() {
		PlayerInventory inv = p.getInventory();
		clearInventory(inv);
		inv.setItem(0, new ItemStack(Material.DIAMOND_SWORD, 1));
		inv.setItem(1, new ItemStack(Material.COOKED_BEEF, 64));
		inv.setItem(2, new ItemStack(Material.DIAMOND_PICKAXE, 1));
		inv.setItem(3, new ItemStack(Material.DIAMOND_SPADE, 1));
		inv.setBoots(new ItemStack(Material.LEATHER_BOOTS));
		inv.setLeggings(new ItemStack(Material.LEATHER_LEGGINGS));
		inv.setChestplate(new ItemStack(Material.LEATHER_CHESTPLATE));
		inv.setHelmet(new ItemStack(Material.LEATHER_HELMET));
	}

	@Override
	public void rightClick(PlayerInteractEvent event) {
		if (p.getItemInHand().getAmount() > 0 && p.getItemInHand().getType() == Material.DIAMOND_SWORD) {
			if (timer.isCooldowned()) {
				effect = new InvisiblityEffect(p, info, 10 * 20 * level, sch);
				timer.start(20 * 20 * level);
				p.sendMessage("You cloak yourself...");
			} else {
				p.sendMessage(timer.getSecondsLeft() + " seconds left to cooldown");
			}
		}

	}

	@Override
	public void playerDamages(LivingEntity damaged) {
		if (effect != null && effect.isRunning()) {
			effect.cancel();
		}
		damaged.addPotionEffect(new PotionEffect(PotionEffectType.POISON, level, level));
	}

	@Override
	public void tookDamage(EntityDamageEvent event) {
		if (event.getCause() == DamageCause.FALL) {
			if (effect != null && effect.isRunning()) {
				event.setCancelled(true);
			}
		}
	}

	@Override
	public boolean entityTargets(Creature e, TargetReason reason) {
		if (effect != null && effect.isRunning()) {
			return false;
		} else {
			return true;
		}
	}

}
