package org.bitbucket.dahlukeh.wizardary.classes.assassin;

import java.util.Collection;
import java.util.List;

import org.bitbucket.dahlukeh.wizardary.gameinfo.GameInfo;
import org.bitbucket.dahlukeh.wizardary.scheduler.Scheduler;
import org.bukkit.entity.Creature;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Monster;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class InvisiblityEffect implements Runnable {

	private Player p;
	private GameInfo info;
	private Scheduler sch;
	private int time;
	private boolean running;

	public InvisiblityEffect(Player p, GameInfo info, int time, Scheduler sch) {
		sch.scheduleOnce(this, time);
		this.sch = sch;
		this.info = info;
		this.p = p;
		this.time = time;
		running = true;
		setUp();
	}

	@Override
	public void run() {
		cancel();
	}

	public void cancel() {
		revert();
		p.sendMessage("You return to sight!");
		sch.cancel(this);
		running = false;
	}

	private void setUp() {
		Collection<Player> people = info.getAllPlayers();
		for (Player pl : people) {
			if (pl != p) {
				pl.hidePlayer(p);
			}
		}

		List<Entity> near = p.getNearbyEntities(20, 20, 20);
		for (Entity e : near) {
			if (e instanceof Monster) {
				Creature c = (Monster) e;
				if (c.getTarget() == p) {
					c.setTarget(null);
				}
			}
		}
		p.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, time, time / 200));
		p.addPotionEffect(new PotionEffect(PotionEffectType.JUMP, time, time / 100));
		p.getWorld().createExplosion(p.getLocation(), 0, false);
	}

	private void revert() {
		Collection<Player> people = info.getAllPlayers();
		for (Player pl : people) {
			if (pl != p) {
				pl.showPlayer(p);
			}
		}
		p.removePotionEffect(PotionEffectType.SPEED);
		p.removePotionEffect(PotionEffectType.JUMP);
	}

	public boolean isRunning() {
		return running;
	}

}
