package org.bitbucket.dahlukeh.wizardary;

import org.bitbucket.dahlukeh.wizardary.classes.BasicPerson;
import org.bitbucket.dahlukeh.wizardary.classes.Person;
import org.bitbucket.dahlukeh.wizardary.classes.archer.ArcherPerson;
import org.bitbucket.dahlukeh.wizardary.classes.assassin.AssassinPerson;
import org.bitbucket.dahlukeh.wizardary.classes.sorcerer.SorcererPerson;
import org.bitbucket.dahlukeh.wizardary.classes.warrior.WarriorPerson;
import org.bitbucket.dahlukeh.wizardary.classes.wizard.WizardPerson;
import org.bitbucket.dahlukeh.wizardary.gameinfo.GameInfo;
import org.bitbucket.dahlukeh.wizardary.scheduler.Scheduler;
import org.bukkit.entity.Player;

public class GeneralFactory implements PersonFactory {

	@Override
	public Person createPerson(Player p, Scheduler scheduler, GameInfo information, String extra) {
		Person newP;
		if (extra.equalsIgnoreCase("wizard")) {
			newP = new WizardPerson(p, scheduler);
		} else if (extra.equalsIgnoreCase("assassin")) {
			newP = new AssassinPerson(p, scheduler, information);
		} else if (extra.equalsIgnoreCase("archer")) {
			newP = new ArcherPerson(p);
		} else if (extra.equalsIgnoreCase("warrior")) {
			newP = new WarriorPerson(p, scheduler);
		} else if (extra.equalsIgnoreCase("sorcerer")) {
			newP = new SorcererPerson(p, scheduler);
		} else {
			newP = new BasicPerson(p);
		}
		newP.setUpPlayer();
		return newP;
	}
	
	

}
