package org.bitbucket.dahlukeh.wizardary.scheduler;

public interface Scheduler {

	public void schedule (Runnable task, int ticksToStart, int ticksToRepeat);
	
	public void scheduleOnce (Runnable task, int ticksToStart);
	
	public void cancel (Runnable task);
	
}
