package org.bitbucket.dahlukeh.wizardary.scheduler;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitScheduler;

public class BasicScheduler implements Scheduler {

	private BukkitScheduler sche;
	private Map<Runnable, Integer> runningTasks;
	private Plugin me;
	
	public BasicScheduler (BukkitScheduler scheduler, Plugin me) {
		sche = scheduler;
		runningTasks = new HashMap<Runnable, Integer>();
		this.me = me;
	}
	
	@Override
	public void schedule(Runnable task, int ticksToStart, int ticksToRepeat) {
		Integer i = sche.scheduleSyncRepeatingTask(me, task, ticksToStart, ticksToRepeat);
		runningTasks.put(task, i);
	}

	@Override
	public void cancel(Runnable task) {
		sche.cancelTask(runningTasks.get(task));
	}

	@Override
	public void scheduleOnce(Runnable task, int ticksToStart) {
		Integer i = sche.scheduleSyncDelayedTask(me, task, ticksToStart);
		runningTasks.put(task, i);
	}

}
