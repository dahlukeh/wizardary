package org.bitbucket.dahlukeh.wizardary;

import org.bitbucket.dahlukeh.wizardary.classes.Person;
import org.bitbucket.dahlukeh.wizardary.gameinfo.GameInfo;
import org.bitbucket.dahlukeh.wizardary.scheduler.Scheduler;
import org.bukkit.entity.Player;


public interface PersonFactory {

	public Person createPerson (Player p, Scheduler scheduler, GameInfo information, String extra);
	
}
