package org.bitbucket.dahlukeh.wizardary.gameinfo;

import java.util.Collection;
import java.util.HashSet;

import org.bukkit.entity.Player;

public class GameInformation implements GameInfo {

	private Collection<Player> people;
	
	public GameInformation () {
		people = new HashSet<Player>();
	}
	
	@Override
	public void addPlayer(Player p) {
		people.add(p);
	}

	@Override
	public void removePlayer(Player p) {
		people.remove(p);
	}

	@Override
	public Collection<Player> getAllPlayers() {
		return people;
	}
	
}
