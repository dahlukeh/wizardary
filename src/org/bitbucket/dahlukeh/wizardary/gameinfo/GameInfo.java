package org.bitbucket.dahlukeh.wizardary.gameinfo;

import java.util.Collection;

import org.bukkit.entity.Player;


public interface GameInfo {

	public void addPlayer (Player p);
	
	public void removePlayer (Player p);
	
	public Collection<Player> getAllPlayers ();
	
}
