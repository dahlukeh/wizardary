package org.bitbucket.dahlukeh.wizardary;

import java.util.ArrayList;
import java.util.List;

import org.bitbucket.dahlukeh.wizardary.scheduler.BasicScheduler;
import org.bitbucket.dahlukeh.wizardary.terrainmaker.SpawnMaker;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;


public class Main extends JavaPlugin {

	private PersonPool pool;
	
	public void onEnable () {
		PluginManager pm = this.getServer().getPluginManager();
		BasicScheduler sch = new BasicScheduler(this.getServer().getScheduler(), this);SpawnMaker maker = new SpawnMaker(this.getServer().getWorlds().get(0));
		maker.make(this.getServer().getWorlds().get(0).getSpawnLocation().add(0, 50, 0), getAllClasses());
		pool = new PersonPool(new GeneralFactory(), sch, maker);
		pm.registerEvents(pool, this);
		
		getLogger().info("Your plugin has been enabled!");
	}
	
	public void onDisable () {
		getLogger().info("Your plugin has been disabled.");
	}
	
	public List<String> getAllClasses () {
		List<String> classes = new ArrayList<String>();
		classes.add("Warrior");
		classes.add("Sorcerer");
		classes.add("Archer");
		classes.add("Wizard");
		classes.add("Assassin");
		return classes;
	}
	
	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
		if (sender instanceof Player) {
			return pool.processCommand((Player) sender, commandLabel, args);
		} else {
			return false;
		}
	}
	
}
